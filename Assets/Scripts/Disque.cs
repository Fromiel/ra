using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Disque : MonoBehaviour
{
    public Disque(Transform o,int t, int p)
    {
        obj = o;
        taille = t;
        pos = p;
    }

    [SerializeField] private Transform obj;
    [SerializeField] private int taille;
    private int pos=0;

    public int Pos
    {
        get => pos;
        set => pos = value;
    }
    public int Taille => taille;
    public Transform Obj => obj;

}
