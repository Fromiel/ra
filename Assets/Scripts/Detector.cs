using TMPro;
using UnityEngine;

public class Detector : MonoBehaviour
{
    private bool _isUpdated;
    
    private TextMeshProUGUI _t1Text;
    private TextMeshProUGUI _t2Text;
    private TextMeshProUGUI _t3Text;

    private GameObject _t1;
    private GameObject _t2;
    private GameObject _t3;

    private void Update()
    {
        if (_isUpdated) return;

        _t1Text = GameObject.Find("text_t1").GetComponent<TextMeshProUGUI>();
        _t2Text = GameObject.Find("text_t2").GetComponent<TextMeshProUGUI>();
        _t3Text = GameObject.Find("text_t3").GetComponent<TextMeshProUGUI>();
        _isUpdated = true;

    }

    [SerializeField] private Material green;
    [SerializeField] private Material blue;
    [SerializeField] private Material red;
    

    private bool _isActivated;
    private int _numTower;
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("t1"))
        {
            if (_isActivated && _numTower == 1)
            {
                _isActivated = false;
                ActivateAll();
            }
            else if(_isActivated && GameManager.Instance.CanMove(_numTower, 1))
            {
                GameManager.Instance.MoveDisque(_numTower, 1);
                _isActivated = false;
                ActivateAll();
            }
            else if(!_isActivated)
            {
                _isActivated = true;
                _numTower = 1;
                SetSelectedAll(1);
            }
        }
        else if (other.CompareTag("t2"))
        {
            if (_isActivated && _numTower == 2)
            {
                _isActivated = false;
                ActivateAll();
            }
            else if(_isActivated && GameManager.Instance.CanMove(_numTower, 2))
            {
                GameManager.Instance.MoveDisque(_numTower, 2);
                _isActivated = false;
                ActivateAll();
            }
            else if(!_isActivated)
            {
                _isActivated = true;
                _numTower = 2;
                SetSelectedAll(2);
            }
        }
        else if(other.CompareTag("t3"))
        {
            if (_isActivated && _numTower == 3)
            {
                _isActivated = false;
                ActivateAll();
            }
            else if(_isActivated && GameManager.Instance.CanMove(_numTower, 3))
            {
                GameManager.Instance.MoveDisque(_numTower, 3);
                _isActivated = false;
                ActivateAll();
            }
            else if(!_isActivated)
            {
                _isActivated = true;
                _numTower = 3;
                SetSelectedAll(3);
            }
        }
    }

    private void ActivateAll()
    {
        _t1.GetComponent<Renderer>().material = green;
        _t2.GetComponent<Renderer>().material = green;
        _t3.GetComponent<Renderer>().material = green;
        _t1Text.text = "Select";
        _t2Text.text = "Select";
        _t3Text.text = "Select";
    }

    private void SetSelectedAll(int tSelected)
    {
        switch (tSelected)
        {
            case 1:
                _t1.GetComponent<Renderer>().material = blue;
                _t2.GetComponent<Renderer>().material = GameManager.Instance.CanMove(1, 2) ? green : red;
                _t3.GetComponent<Renderer>().material = GameManager.Instance.CanMove(1, 3) ? green : red;
                _t1Text.text = "Return";
                _t2Text.text = GameManager.Instance.CanMove(1, 2) ? "Move here" : "Can't move";
                _t3Text.text = GameManager.Instance.CanMove(1, 3) ? "Move here" : "Can't move";
                break;
            case 2:
                _t2.GetComponent<Renderer>().material = blue;
                _t1.GetComponent<Renderer>().material = GameManager.Instance.CanMove(2, 1) ? green : red;
                _t3.GetComponent<Renderer>().material = GameManager.Instance.CanMove(2, 3) ? green : red;
                _t2Text.text = "Return";
                _t1Text.text = GameManager.Instance.CanMove(2, 1) ? "Move here" : "Can't move";
                _t3Text.text = GameManager.Instance.CanMove(2, 3) ? "Move here" : "Can't move";
                break;
            case 3:
                _t3.GetComponent<Renderer>().material = blue;
                _t2.GetComponent<Renderer>().material = GameManager.Instance.CanMove(3, 2) ? green : red;
                _t1.GetComponent<Renderer>().material = GameManager.Instance.CanMove(3, 1) ? green : red;
                _t3Text.text = "Return";
                _t2Text.text = GameManager.Instance.CanMove(3, 2) ? "Move here" : "Can't move";
                _t1Text.text = GameManager.Instance.CanMove(3, 1) ? "Move here" : "Can't move";
                break;
        }
    }

    
}
