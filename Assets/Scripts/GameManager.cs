using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : Singleton<GameManager>
{
    
    [SerializeField] private Disque[] disques1234 = new Disque[4];
    [SerializeField] private Transform[] pilliers = new Transform[3];
    [SerializeField] private Canvas winScreen;
    private Vector3[] pos_pillier = new Vector3[3];
    private int nombre_disques_fin = 0;
    private Stack<Disque>[] plateau = new Stack<Disque>[3];
    private void Start()
    {
        pos_pillier[0] = pilliers[0].position;
        pos_pillier[1] = pilliers[1].position;
        pos_pillier[2] = pilliers[2].position;

        for(int i=0; i < 4; i++)
        {
            if (i < 3)
            {
                plateau[i] = new Stack<Disque>();
            }
            plateau[0].Push(disques1234[3-i]);
        }
        print(plateau[0]);
    }

    private void Win()
    {
        Debug.Log("Win WIn tu as gagn�");
        winScreen.enabled = true;
    }
    public bool CanMove(int pos1,int pos2)
    {
        pos1 -=1;
        pos2 -= 1;
        if (plateau[pos1].Count > 0)
        {
            Disque disqueABouger = plateau[pos1].Peek();
            if (plateau[pos2].Count == 0 || plateau[pos2].Peek().Taille > disqueABouger.Taille)
            {
                return true;
            }
        }
        return false;
    }
    public bool MoveDisque(int pos1, int pos2)
    {
        pos1 -= 1;
        pos2 -= 1;
        if (plateau[pos1].Count > 0)
        {
            Disque disqueABouger = plateau[pos1].Peek();
            if (plateau[pos2].Count == 0 || plateau[pos2].Peek().Taille > disqueABouger.Taille)
            {
                disqueABouger.Pos = pos2;
                plateau[pos1].Pop();
                plateau[pos2].Push(disqueABouger);
                disqueABouger.Obj.position = new Vector3(pos_pillier[pos2].x, disqueABouger.Obj.position.y + 0.2f, pos_pillier[pos2].z);
                if (pos1 == 2)
                {
                    nombre_disques_fin -= 1;
                }
                if (pos2 == 2)
                {
                    nombre_disques_fin += 1;
                }
                if (nombre_disques_fin == 4)
                {
                    Win();
                }
                Debug.Log(nombre_disques_fin);
                return true;
            }
        }
        return false;
    }

    public void restart()
    {
        SceneManager.LoadScene(0);
    }
}
