using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{
    private int selected_bar=-1;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            inputaction(1);
        }
        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            inputaction(2);
        }
        if (Input.GetKeyDown(KeyCode.Keypad3))
        {
            inputaction(3);
        }
    }

    private void inputaction(int a)
    {
        Debug.Log(a);
        if (selected_bar == a)
        {
            selected_bar = -1;
        }
        else
        {
            if (selected_bar != -1)
            {
                if (GameManager.Instance.MoveDisque(selected_bar, a))
                {
                    selected_bar = -1;
                    print("success");
                }
                else
                {
                    selected_bar = -1;
                }
            }
            else
            {
                selected_bar = a;
            }
        }
    }
}

