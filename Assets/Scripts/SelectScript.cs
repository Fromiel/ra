using TMPro;
using UnityEngine;

public class SelectScript : MonoBehaviour
{
    private bool _isSelected;
    private int _numTSelected;

    [SerializeField] Outline o1;
    [SerializeField] private Outline o2;
    [SerializeField] private Outline o3;


    public void Click()
    {
        var transform1 = transform;
        var ray = new Ray(transform1.position, transform1.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 500))
        {
            if (hit.transform.gameObject.CompareTag("t1"))
            {
                if (_isSelected)
                {
                    _isSelected = false;
                    hit.transform.gameObject.GetComponent<Outline>().OutlineWidth = 0;
                    GameManager.Instance.MoveDisque(_numTSelected, 1);
                    Desoutline();
                }
                else
                {
                    _numTSelected = 1;
                    _isSelected = true;
                    hit.transform.gameObject.GetComponent<Outline>().OutlineWidth = 6;
                }
            }
            else if (hit.transform.gameObject.CompareTag("t2"))
            {
                if (_isSelected)
                {
                    _isSelected = false;
                    hit.transform.gameObject.GetComponent<Outline>().OutlineWidth = 0;
                    GameManager.Instance.MoveDisque(_numTSelected, 2);
                    Desoutline();
                }
                else
                {
                    _numTSelected = 2;
                    _isSelected = true;
                    hit.transform.gameObject.GetComponent<Outline>().OutlineWidth = 6;
                }
            }
            else if (hit.transform.gameObject.CompareTag("t3"))
            {
                if (_isSelected)
                {
                    _isSelected = false;
                    hit.transform.gameObject.GetComponent<Outline>().OutlineWidth = 0;
                    GameManager.Instance.MoveDisque(_numTSelected, 3);
                    Desoutline();
                }
                else
                {
                    _numTSelected = 3;
                    _isSelected = true;
                    hit.transform.gameObject.GetComponent<Outline>().OutlineWidth = 6;

                }
            }
        }
    }

    void Desoutline()
    {
        switch (_numTSelected)
        {
            case 1:
                o1.OutlineWidth = 0;
                break;
            case 2:
                o2.OutlineWidth = 0;
                break;
            case 3:
                o3.OutlineWidth = 0;
                break;
        }
    }
}
