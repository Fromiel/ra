using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ImageTrackingObjectManager : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Image manager on the AR Session Origin")]
    private ARTrackedImageManager imageManager;
    
    
    [SerializeField]
    [Tooltip("Reference Image Library")]
    private XRReferenceImageLibrary imageLibrary;


    [SerializeField] private TextMeshProUGUI baseText;
    [SerializeField] private TextMeshProUGUI t1Text;
    [SerializeField] private TextMeshProUGUI t2Text;
    [SerializeField] private TextMeshProUGUI t3Text;

    private List<ARTrackedImage> _images;


    #region imagesPrefab
    
    [SerializeField]
    [Tooltip("Prefab for tracked game board image")]
    private GameObject basePrefab;

    private GameObject _spawnedBasePrefab;


    [SerializeField]
    [Tooltip("Prefab for tracked first tower image")]
    GameObject firstTowerPrefab;
    
    GameObject _spawnedFirstTowerPrefab;
    
    
    [SerializeField]
    [Tooltip("Prefab for tracked first tower image")]
    private GameObject secondTowerPrefab;

    private GameObject _spawnedSecondTowerPrefab;
    
    
    [SerializeField]
    [Tooltip("Prefab for tracked first tower image")]
    private GameObject thirdTowerPrefab;
    
    private GameObject _spawnedThirdTowerPrefab;

    #endregion

    private int _numberOfTrackedImages;
        

    private static Guid _sFirstImageGuid;
    private static Guid _sSecondImageGuid;
    private static Guid _sThirdImageGuid;
    private static Guid _sFourthImageGuid;

    void OnEnable()
    {
        _sFirstImageGuid = imageLibrary[0].guid;
        _sSecondImageGuid = imageLibrary[1].guid;
        _sThirdImageGuid = imageLibrary[2].guid;
        _sFourthImageGuid = imageLibrary[3].guid;
        
        imageManager.trackedImagesChanged += ImageManagerOnTrackedImagesChanged;
    }

    void OnDisable()
    {
        imageManager.trackedImagesChanged -= ImageManagerOnTrackedImagesChanged;
    }

    void ImageManagerOnTrackedImagesChanged(ARTrackedImagesChangedEventArgs obj)
    {
        // added, spawn prefab
        foreach(ARTrackedImage image in obj.added)
        {
            if (image.referenceImage.guid == _sFirstImageGuid)
            {
                baseText.text = "base : detected";
                var transform1 = image.transform;
                _spawnedBasePrefab = Instantiate(basePrefab, transform1.position, transform1.rotation);
                _images.Add(image);
            }
            else if (image.referenceImage.guid == _sSecondImageGuid)
            {
                t1Text.text = "t1 : detected";
                var transform1 = image.transform;
                _spawnedFirstTowerPrefab = Instantiate(firstTowerPrefab, transform1.position, transform1.rotation);
                _images.Add(image);
            }
            else if (image.referenceImage.guid == _sThirdImageGuid)
            {
                t2Text.text = "t2 : detected";
                var transform1 = image.transform;
                _spawnedSecondTowerPrefab = Instantiate(secondTowerPrefab, transform1.position, transform1.rotation);
                _images.Add(image);
            }
            else if (image.referenceImage.guid == _sFourthImageGuid)
            {
                t3Text.text = "t3 : detected";
                var transform1 = image.transform;
                _spawnedThirdTowerPrefab = Instantiate(thirdTowerPrefab, transform1.position, transform1.rotation);
                _images.Add(image);
            }
            
            
        }
        
        // updated, set prefab position and rotation
        foreach(ARTrackedImage image in obj.updated)
        {
            // image is tracking or tracking with limited state, show visuals and update it's position and rotation
            if (image.trackingState == TrackingState.Tracking)
            {
                if (image.referenceImage.guid == _sFirstImageGuid)
                {
                    var transform1 = image.transform;
                    _spawnedBasePrefab.transform.SetPositionAndRotation(transform1.position, transform1.rotation);
                }
                else if (image.referenceImage.guid == _sSecondImageGuid)
                {
                    var transform1 = image.transform;
                    _spawnedFirstTowerPrefab.transform.SetPositionAndRotation(transform1.position, transform1.rotation);
                }
                else if (image.referenceImage.guid == _sThirdImageGuid)
                {
                    var transform1 = image.transform;
                    _spawnedSecondTowerPrefab.transform.SetPositionAndRotation(transform1.position, transform1.rotation);
                }
                else if (image.referenceImage.guid == _sFourthImageGuid)
                {
                    var transform1 = image.transform;
                    _spawnedThirdTowerPrefab.transform.SetPositionAndRotation(transform1.position, transform1.rotation);
                }
                UpdateARObjects(image);
            }
        }
        
        // removed, destroy spawned instance
        foreach(ARTrackedImage image in obj.removed)
        {
            if (image.referenceImage.guid == _sFirstImageGuid)
            {
                baseText.text = "base : not detected";
                Destroy(_spawnedBasePrefab);
            }
            else if (image.referenceImage.guid == _sSecondImageGuid)
            {
                t1Text.text = "t1 : not detected";
                Destroy(_spawnedFirstTowerPrefab);
            }
            else if (image.referenceImage.guid == _sThirdImageGuid)
            {
                t2Text.text = "t2 : not detected";
                Destroy(_spawnedSecondTowerPrefab);
            }
            else if (image.referenceImage.guid == _sFourthImageGuid)
            {
                t3Text.text = "t3 : not detected";
                Destroy(_spawnedThirdTowerPrefab);
            }
        }
    }
    
    void UpdateARObjects(ARTrackedImage image)
    {

        if (image.trackingState != TrackingState.Tracking) 
        {
            if (image.referenceImage.guid == _sFirstImageGuid)
            {
                baseText.text = "base : not detected";
            }
            else if (image.referenceImage.guid == _sSecondImageGuid)
            {
                t1Text.text = "t1 : not detected";
            }
            else if (image.referenceImage.guid == _sThirdImageGuid)
            {
                t2Text.text = "t2 : not detected";
            }
            else if (image.referenceImage.guid == _sFourthImageGuid)
            {
                t3Text.text = "t3 : not detected";
            }
        } else
        {
            if (image.referenceImage.guid == _sFirstImageGuid)
            {
                baseText.text = "base : detected";
            }
            else if (image.referenceImage.guid == _sSecondImageGuid)
            {
                t1Text.text = "t1 : detected";
            }
            else if (image.referenceImage.guid == _sThirdImageGuid)
            {
                t2Text.text = "t2 : detected";
            }
            else if (image.referenceImage.guid == _sFourthImageGuid)
            {
                t3Text.text = "t3 : detected";
            }
        }
    }

    private void Update()
    {
        
        foreach (var image in imageManager.trackables)
        {
            UpdateARObjects(image);
        }
    }

    public int NumberOfTrackedImages()
    {
        _numberOfTrackedImages = 0;
        foreach (ARTrackedImage image in imageManager.trackables)
        {
            if (image.trackingState == TrackingState.Tracking)
            {
                _numberOfTrackedImages++;
            }
        }
        return _numberOfTrackedImages;
    }
}